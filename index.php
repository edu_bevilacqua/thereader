<?php
    include("processaLogin.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <script src="script/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>The Reader</title>
</head>
<body>
    <img src="img/logo-thereader.png" alt="The Reader" id="logo-login">
    <div class="container">
        <form action=" " method="POST">
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email" class="form-control" name='email' id="email-login" placeholder="Digitar o e-mail aqui">
                <small id="emailHelp" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" class="form-control" name='password' id="senha-login" placeholder="Digitar a senha aqui">
            </div>
            <button type="submit" class="btn btn-dark btn-lg">Entrar</button>
            <button type="button" class="btn btn-outline-dark btn-lg">Cadastrar</button>
        </form><br>
        <p><a>Esqueci minha senha<a></p>
        <?php
            if(empty($error)==FALSE){
                echo '<div class="alert alert-danger" role="alert">E-mail ou senha incorretos!</div>';
            }
        ?>
    </div>
</body>