<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="script/script.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>The Reader</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img id="logo" src="~/img/logo-thereader.png" width="auto" height="30px" alt="The Reader">
        </a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Últimos Lançamentos<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Mais Lidos</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categorias
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Ação</a>
                <a class="dropdown-item" href="#">Aventura</a>
                <a class="dropdown-item" href="#">Comédia</a>
                <a class="dropdown-item" href="#">Romance</a>
                <a class="dropdown-item" href="#">Policial</a>
                <a class="dropdown-item" href="#">Auto Ajuda</a>
                <a class="dropdown-item" href="#">Economia</a>
                <a class="dropdown-item" href="#">Mais...</a>
                </div>
            </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="" aria-label="Search">
            <button class="btn btn-dark my-2 my-sm-0" type="submit">Pesquisar</button>
            </form>
        </div>
    </nav>
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <img src="img/capa.jpg" alt="Não Disponível" height="250px" width="auto" class="float-right">
            </div>
            <div class="col-4">
                <h2>As Seis Lições</h2>
                <h4>Ludwig Von Mises</h4>
                <h5>2011 - 7ª edição</h5>
            </div>
            <div class="col-4">
                <h4>Nota</h4>
                <h1>9.4</h1>
                <button class="btn btn-dark my-2 my-sm-0" data-toggle="modal" data-target="#exampleModalCenter">
                    Avaliar
                </button>
                <!-- Modal -->
                <div class="modal fade" id="ModalAvaliar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="TituloModal">Escolha a nota do livro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mb-3">
                            <select class="custom-select" id="notaModal">
                                <option selected>Nota</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary">Confirmar</button>
                    </div>
                    </div>
                </div>
                </div>     
        </div>
        <br><br>
        <h2>Descrição do livro</h2>
        <p>Síntese da obra do grande economista Ludwig von Mises, talvez o maior da história, As seis lições pode ser considerado a melhor introdução ao pensamento do mestre e um grande resumo de sua extensa produção acadêmica. Trata-se de um livro curto: em poucas palavras, ele demonstra a perenidade de seus conceitos a respeito de política econômica, como propriedade privada, livre comércio, preços, juros, moeda e inflação, bem como os de sistemas político-econômicos, como capitalismo, intervencionismo, socialismo, investimento externo. Fruto de palestras dirigidas a empresários, professores e alunos, As seis lições foi organizado pela viúva de Mises, é um clássico da ciência econômica em linguagem acessível para o público leigo e é provavelmente o livro mais vendido do autor.</p>
    </div>
</body>
</html>